// console.log("Hello World");

// Array Methods
// JS has built-in functions and methods for arrays.

// Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
// will add element to the end part of the array
// SYNTAX: arrayName.push()
console.log("Current Array:");
console.log(fruits);
let fruitLength = fruits.push("Mango");
console.log(fruitLength);
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

// pop()
// will remove array element to the end part of the array
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// unshift();
// add element in the beginning of an array

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift()
// will remove an element in the begining part of an array

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// splice()
// Simultaneously removes an element from a specified index number and adds elements
// SYNTAX -> arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method")
console.log(fruits);

// sort()
// Re-arranges the array elements in alphanumeric order

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse()
// Reverses order of array elements

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

// Non-Mutator Methods
// unlike the mutator methods, non-mutator methods cannot modify array.

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

// indexOf()
// Will return the index number of the first matching element
// if no element was found, JS will return -1
// Syantax -> arrayName.indoxOf(searchValue, fromIndex)

let firstIndex = countries.indexOf("PH");
console.log("Result of index method: " + firstIndex);

let invalidIndex = countries.indexOf("BR");
console.log("Result of index method: " + invalidIndex);

// lastIndexOf()
// Returns index number of the last matching element in an array
// Syntax -> arrayName.lastIndexOf(searchValue, fromIndex)

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

// Getting the index nummber starting from specified index
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexStart method: " + lastIndexStart);

// slice()
// Portions/slices elements from an array and returna new array
// Syntax -> arrayName.slice(startingIndex, endingIndex)
let slicedArrayA = countries.slice(2);
console.log("Result from slice method");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method");
console.log(slicedArrayB);

// slicing off elements starting from the last elements of an array
let slicedArrayC = countries.slice(-3)
console.log("Result from slice method");
console.log(slicedArrayC);

// toString()
// Returns an array as a string separated by commas
// Syntax -> arrayName.toString()

let stringArray = countries.toString();
console.log("Result from string method");
console.log(stringArray);

// concat()
// Combine two arrays and returns the combined result
// Syntax -> arrayA.concat(arrayB)

let taskArray1 = ["drink html", "eat js"];
let taskArray2 = ["inhale css", "breathe sass"];
let taskArray3 = ["get git", "be node"];

let tasks = taskArray1.concat(taskArray2);
console.log("Result from concat method");
console.log(tasks);

// combining multiple arrays
console.log("Result from concat method");
let allTasks = taskArray1.concat(taskArray2, taskArray3);
console.log(allTasks);

// Combining arrays with elements
console.log("Result from concat method");
let combinedTasks = taskArray1.concat("smell express", "throw react");
console.log(combinedTasks);

// join()
// Returns an array as a string seperated by specified separator string
// Syntax -> arrayName.Join('separatorString')

let users = ["John", "Jane", "Joe", "Robert"];

console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration Methods

// forEach()
// Similar to a for loop that iterates on each array elements
// Syntax -> arrayName.forEach(function(indivElement)){statement} 

allTasks.forEach(function(task){
	console.log(task)
});

// Using forEach with conditional statement

let filteredTasks = [];
allTasks.forEach(function(task){
	if(task.length >= 10){
		filteredTasks.push(task);
	}
});

console.log("Result of filtered tasks");
console.log(filteredTasks);

// map()
// This is useful for performing tasks where mutating/changing the elements are required.
// Syntax -> let/const resultArray = arrayName.map(function(indivElement))

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
});

console.log("Original Array:");
console.log(numbers); // original is unaffected
console.log("Result from map method");
console.log(numberMap); // new array is returned and stored in a variable

// map() vs forEach
let numberForEach = numbers.forEach(function(number){
	return number * number
})

console.log(numberForEach);

// forEach(), loops over all items in the array as map(), but forEach() does not return new array.

// every()
// checks if all elements in an array meet the given condition
// Syntax -> let/const resultArray = arrayName.every(function(undivElement){condition})

let allValid = numbers.every(function(number){
	return (number < 6);
});
console.log("Result from every method");
console.log(allValid);

// some()
// checks if at least one element in array meets the condition
// Syntax -> let/const resultArray = arrayName.some(function(undivElement){condition})

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log("Result from some method");
console.log(someValid);

// filter()
// Returns new array that contains elements which meet the gven condition
// Return an empty array if no elemenets were found
// Syntax -> let/const resultArray = arrayName.filter(function(indivElement){condition})

let filterValid = numbers.filter(function(number){
	return (number < 3)
});
console.log("Result from filter method");
console.log(filterValid);

// includes()
// Checks if the argument passed can be found in the array
// Syntax -> array.Name.includes(<argumentToFind>)

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1)

let productFound2 = products.includes("Headset");
console.log(productFound2);

let filteredProducts = products.filter(function(product){
	return (product.toLowerCase().includes("a"));
})
console.log(filteredProducts);

// reduce()
// Evaluates elements from left to right and returns/reduces the array into a single value.
// Syntax -> let/const result Array = arrayName.reduce(function(accumulator, currentValue){operation})

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current Iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue" + y);

	// The operation to reduce the array into single value
	return x + y;
})

console.log("Result of reduce method: " + reducedArray);

let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x, y){
	return x + " " + y;
})

console.log("Result of reduced method: " + reducedJoin)


